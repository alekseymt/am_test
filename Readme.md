# Test task

Solution is provided in docker with redis instance and load balanced using `pm2` clustering.
Time param supposed to be in an ISO format.

### Requirements

- `docker`
- `docker-compose`

### Installation

1. Clone this project to local environment
1. Run `docker-compose up -d`
1. Now endpoint is available at http://0.0.0.0:8080/echoAtTime
