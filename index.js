const bodyParser = require('body-parser');
const moment = require('moment');
const express = require('express');
const QueueAdapter = require('./services/QueueAdapter');
const app = express();

const Queue = new QueueAdapter();

Queue.processExpired();

app.use(bodyParser.urlencoded({ extended: true }));

app.post('/echoAtTime', (req, res) => {
  const message = req.body.message;
  const time = moment(req.body.time).unix();

  Queue.addToQueue(message, time).then(() => {
    res.json({ success: true, message: "Message successfully added to the queue." });
  });
});

app.use(function (req, res, next) {
  res.status(404).json({ success: false, message: "Not found." });
})

app.listen(process.env.APP_PORT, () => console.log(`Listening on port ${process.env.APP_PORT}.`));
