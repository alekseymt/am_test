const uuid = require('uuid/v1');
const redis = require("redis");
const moment = require('moment');

function QueueAdapter() {
  this.client = redis.createClient(process.env.APP_REDIS_PORT, process.env.APP_REDIS_HOST);
  this.subscriber = redis.createClient(process.env.APP_REDIS_PORT, process.env.APP_REDIS_HOST);

  this.setConfig();
  this.addListeners();
}

QueueAdapter.prototype.setConfig = function() {
  this.client.config("SET", "notify-keyspace-events", "KEA");
}

QueueAdapter.prototype.addListeners = function() {
  this.subscriber.on('pmessage', (pattern, channel, message) => {
    const key = message.split(":")[1];

    this.processMessage(key);
  });

  this.subscriber.psubscribe("*:expired");
}

QueueAdapter.prototype.processExpired = function() {
  this.client.zrevrangebyscore([ 'queue', moment().unix(), 0, 'WITHSCORES', 'LIMIT', 0, -1 ], (err, queue) => {
    while (queue.length){
        const key = queue.shift(),
              timestamp = queue.shift();

        this.processMessage(key);
    }
  });
}

QueueAdapter.prototype.addToQueue = function(message, time) {
  const key = uuid();
  const delay = time - moment().unix();
  const expiresIn = delay > 1 ? delay : 1;

  return new Promise((resolve, reject) => {
    this.client.set(`notifications:${key}`, message, 'EX', expiresIn);
    this.client.hset('messages', key, message);
    this.client.zadd('queue', time, key);

    resolve();
  });
}

QueueAdapter.prototype.processMessage = function(key) {
  const redisCall = new Promise((resolve, reject) => {
    this.client.hget('messages', key, (err, message) => {
      resolve(message);
    });
  });

  redisCall
    .then((message) => {
      return new Promise((resolve, reject) => {
        this.client.hdel('messages', key, (err, status) => {
          if (status === 1) {
            resolve(message);
          }
        });
      });
    })
    .then((message) => {
      this.client.zrem('queue', key);
      console.log(message);
    });
}

module.exports = QueueAdapter;
